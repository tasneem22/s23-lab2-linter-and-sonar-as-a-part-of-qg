
[![pipeline status](https://gitlab.com/tasneem22/s23-lab2-linter-and-sonar-as-a-part-of-qg/badges/master/pipeline.svg)](https://gitlab.com/tasneem22/s23-lab2-linter-and-sonar-as-a-part-of-qg/-/commits/master)

# Lab 2 -- Linter and SonarQube as a part of quality gates


## Lab Results
   ![sonarqube-scan](./images/sonarqube-scan.png)

## Homework

added gitlab-ci part of sonarCloud
![sonarcloud](./images/sonarcloud.png)
